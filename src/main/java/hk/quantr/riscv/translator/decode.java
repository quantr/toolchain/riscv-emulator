package hk.quantr.riscv.translator;

/* import hk.quantr.assembler.exception.NoOfByteException;
import hk.quantr.assembler.RISCVDisassembler;
import hk.quantr.assembler.exception.WrongInstructionException;
import hk.quantr.assembler.riscv.il.Line;
import hk.quantr.assembler.riscv.il.Registers;
import hk.quantr.dwarf.QuantrDwarf;
import hk.quantr.javalib.CommonLib;
import static hk.quantr.riscv.emulator.Emulator.registers;
import hk.quantr.riscv_simulator.cpu.CPUState;
import hk.quantr.riscv_simulator.exception.PageFaultException;
import hk.quantr.riscv_simulator.exception.InterruptException;
import hk.quantr.riscv_simulator.exception.AccessFaultException;
import hk.quantr.riscv_simulator.exception.IRQException;
import java.math.BigInteger;
import java.util.ArrayList; */

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author yin
 */
/* public class decode {
	
	private static final Logger logger = Logger.getLogger(Simulator.class.getName());
	//System structuring
	public static LinkedHashMap<String, Register> registers = new LinkedHashMap<>();
	HashSet<MemoryHandler> memoryHandlers = new HashSet<>();
	public Memory memory = new Memory(this, memoryHandlers);
	public int Harts = 1;
	//System variables
	RISCVDisassembler riscvDisassembler = new RISCVDisassembler();
	LinkedHashMap<String, Integer> bp = new LinkedHashMap<>();
	public CommandLine cmd;
	int bp_flag = 0, startAddress;
	String dumpJson;
	boolean isXml, Startcont = false, interrupt = false;
	Thread c2;
	long cpuTick = 0;
	ArrayList<Dwarf> dwarfArrayList;
	private boolean printDisassembleLine;
	public long mtime;
	public long mtimecmp;
	LogInterface logThread;

	public void executeCPU() {
		int[] opcodes = null;
		try {
			Line line = null;

			int noOfByte = RISCVDisassembler.getNoOfByte(memory.read(pc, true, false) & 0x7f);
			String type = null;
			riscvDisassembler.setCurrentOffset(pc);
			if (bp.get(Long.toHexString(pc)) != null) {
				if (bp.get(Long.toHexString(pc)) == 1) {
					System.out.println("Breakpoint Hit!");
					bp_flag = 1;
					bp.put(Long.toHexString(pc), 0);
				}
			}
			if (noOfByte == 4) {
				opcodes = new int[4];
				opcodes[0] = memory.read(pc, true, false) & 0xff;
				opcodes[1] = memory.read(pc + 1, true, false) & 0xff;
				opcodes[2] = memory.read(pc + 2, true, false) & 0xff;
				opcodes[3] = memory.read(pc + 3, true, false) & 0xff;
			} else if (noOfByte == 2) {
				opcodes = new int[2];
				opcodes[0] = memory.read(pc, true, false) & 0xff;
				opcodes[1] = memory.read(pc + 1, true, false) & 0xff;
			} else {
				opcodes = new int[1];
				opcodes[0] = 99;
			}
			type = RISCVDisassembler.getType(memory.read(pc, true, false) & 0x7F);
			handleTimer();
			if (type.equals("decodeTypeB")) {
				line = riscvDisassembler.decodeTypeB(opcodes);
				if (bp_flag == 0) {
					handleTypeB(line);
				}
			} else if (type.equals("decodeTypeI")) {
				line = riscvDisassembler.decodeTypeI(opcodes);
				if (bp_flag == 0) {
					handleTypeI(opcodes, line);
				}
			} else if (type.equals("decodeTypeS")) {
				line = riscvDisassembler.decodeTypeS(opcodes);
				if (bp_flag == 0) {
					handleTypeS(line);
				}
			} else if (type.equals("decodeTypeR")) {
				line = riscvDisassembler.decodeTypeR(opcodes);
				if (bp_flag == 0) {
					handleTypeR(line);
				}
			} else if (type.equals("decodeTypeU")) {
				line = riscvDisassembler.decodeTypeU(opcodes);
				if (bp_flag == 0) {
					handleTypeU(line);
				}
			} else if (type.equals("ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU")) {
				line = riscvDisassembler.ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU(opcodes);
				if (bp_flag == 0) {
					handleEcallOrTrap(line);
				}
			} else if (type.equals("jal")) {
				line = riscvDisassembler.jal(opcodes);
				if (bp_flag == 0) {
					String rd = Registers.getRegXNum32(line.rd);
					long long_val = registers.get("pc").getValue().longValue() + 4;
					if (!rd.equals("zero")) {
						registers.get(rd).setValue(long_val);
					}
					registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				}
			} else if (type.equals("jalr")) {
				line = riscvDisassembler.jalr(opcodes);
				if (bp_flag == 0) {
					String rs1 = Registers.getRegXNum32(line.rs1);
					long rsValue = registers.get(rs1).getValue().longValue();
					if (line.rd != 0) {
						String rd = Registers.getRegXNum32(line.rd);
						registers.get(rd).setValue(registers.get("pc").getValue().longValue() + 4);
					}
					registers.get("pc").setValue(rsValue + line.imm);
				}
			} else if (type.equals("fence")) {
				line = riscvDisassembler.fence(opcodes);
				if (bp_flag == 0) {
					modifyPC();
				}
			} else if (type.equals("cityu")) {
				line = riscvDisassembler.cityu(opcodes);
				if (bp_flag == 0) {
					handleCityu(line);
				}
			} else if (type.equals("magic")) {
				line = riscvDisassembler.decodeMagic(opcodes);
				bp_flag = 1;
				Startcont = true;
				modifyPC();
			} else if (type.equals("compressed")) {
				if (opcodes[0] == 0 && opcodes[1] == 0) {
					System.out.println("Bad bytes, exit, pc=" + registers.get("pc").getHexString());
					return;
				} else {
					line = riscvDisassembler.decodeCompressed(opcodes);
					if (bp_flag == 0) {
						handleCompressed(line);
					}
				}
			} else if (type.equals("decodeTypeV")) {
				line = riscvDisassembler.decodeTypeV(opcodes);
				if (bp_flag == 0) {
					handleTypeV(opcodes, line);
				}
			}
			if (printDisassembleLine) {
				if (dwarfArrayList == null) {
					System.out.println(String.format("%5d, 0x%08x, [%-19s]: %s", cpuTick, pc, CommonLib.arrayToHexString(opcodes), line.code));
				} else {
					ArrayList<String> s = QuantrDwarf.getCCode(dwarfArrayList, BigInteger.valueOf(pc), false);
					if (s != null) {
						for (String temp : s) {
							System.out.println(ConsoleColor.blue + temp);
						}
					}
					System.out.println(String.format("%5d, 0x%08x, [%-19s]: %s", cpuTick, pc, CommonLib.arrayToHexString(opcodes), line.code));
				}
			}
			mtime += 1;
			memory.write(0x200bff8, mtime, 8, true, false);
		} catch (PageFaultException | IRQException ex) {
			registers.get("mepc").setValue(registers.get("pc").getValue().longValue());
			registers.get("pc").setValue(registers.get("mtvec").getValue().longValue() & 0xfffffffffffffffcL);
			registers.get("mcause").setValue(0xd); //this is wrong
			CPUState.setPriv(3);
		} catch (InterruptException ex) {
			long address = registers.get("pc").getValue().longValue();
			long value = 0;
			if (CPUState.priv == CPUState.MACHINE_MODE) {
				value = registers.get("mtvec").getValue().longValue();
				registers.get("mcause").setValue(BigInteger.valueOf(ex.fireInterrupt.cause).or(new BigInteger("8000000000000000", 16)));
				registers.get("mepc").setValue(address);
			} else if (CPUState.priv == CPUState.SUPERVISOR_MODE) {
				value = registers.get("stvec").getValue().longValue();
				registers.get("scause").setValue(BigInteger.valueOf(ex.fireInterrupt.cause).or(new BigInteger("8000000000000000", 16)));
				registers.get("sepc").setValue(address + opcodes.length);

				BigInteger mstatus = registers.get("mstatus").getValue();
				mstatus = mstatus.or(BigInteger.valueOf(0x120));
//				mstatus = mstatus.and(BigInteger.valueOf(0xfffffffffffffffdl));
				registers.get("mstatus").setValue(mstatus);

				BigInteger sstatus = registers.get("sstatus").getValue();
				sstatus = sstatus.or(BigInteger.valueOf(0x120));
				registers.get("sstatus").setValue(sstatus);
			} else if (CPUState.priv == CPUState.USER_MODE) {
				value = registers.get("utvec").getValue().longValue();
//				registers.get("ucause").setValue(BigInteger.valueOf(ex.fireInterrupt.cause).or(new BigInteger("8000000000000000", 16)));
				registers.get("uepc").setValue(address + opcodes.length);

				BigInteger ustatus = registers.get("ustatus").getValue();
				ustatus = ustatus.or(BigInteger.valueOf(0x120));
				ustatus = ustatus.and(BigInteger.valueOf(0xfffffffffffffffdl));
				registers.get("ustatus").setValue(ustatus);
			} else {
				System.out.println("fuck interrupt");
				System.exit(123);
			}
			registers.get("pc").setValue(value);

//			long mstatus = registers.get("mstatus").value.longValue();
//			if (CPUState.priv == CPUState.SUPERVISOR_MODE) {
//				mstatus = mstatus | 0x120;
//			}
//			registers.get("mstatus").setValue(mstatus);
			registers.get("mip").setValue(registers.get("mip").getValue().longValue() | 0x80);
			CPUState.setPriv(3);
		} catch (WrongInstructionException | NoOfByteException ex) {

		} catch (AccessFaultException ex) {
			System.exit(6000);
		}
		cpuTick++;
	}
} */
