/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hk.quantr.riscv.translator;

import hk.quantr.assembler.exception.NoOfByteException;
import hk.quantr.assembler.riscv.il.Line;
import hk.quantr.assembler.riscv.il.Registers;
import hk.quantr.javalib.CommonLib;
import java.math.BigInteger;
import java.util.logging.Level;

/**
 *
 * @author yin
 */
public class instructionHandler {
	
	/* private void handleTypeI(int arr[], Line line) throws PageFaultException, NoOfByteException, IRQException, AccessFaultException {
		String rd = Registers.getRegXNum32(line.rd);
		String rs1 = Registers.getRegXNum32(line.rs1);
		int shamt5 = (arr[3] & 0x1) << 4 | (arr[2] & 0xf0) >> 4;
		int shamt6 = (arr[3] & 0x3) << 4 | (arr[2] & 0xf0) >> 4;
		long long_val;
		if (line.instruction.equals("addi")) {
			if (rs1.equals("zero")) {
				registers.get(rd).setValue(signExtend64(line.imm, 11));
			} else {
				long_val = registers.get(rs1).getValue().longValue() + signExtend64(line.imm, 11);
				registers.get(rd).setValue(long_val);
			}
		} else if (line.instruction.equals("andi")) {
			long_val = registers.get(rs1).getValue().longValue() & signExtend64(line.imm, 11);
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("ori")) {
			long_val = registers.get(rs1).getValue().longValue() | signExtend64(line.imm, 11);
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("xori")) {
			long_val = registers.get(rs1).getValue().longValue() ^ line.imm;
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("slli")) {
			long_val = registers.get(rs1).getValue().longValue() << shamt6;
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("srli")) {
			long_val = registers.get(rs1).getValue().longValue() >>> shamt6;
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("srai")) {
			long_val = registers.get(rs1).getValue().longValue() >> shamt6;
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("slliw")) {
			long_val = registers.get(rs1).getValue().longValue() << shamt5;
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("srliw")) {
			long_val = registers.get(rs1).getValue().longValue() >>> shamt5;
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("sraiw")) {
			long_val = registers.get(rs1).getValue().longValue() >> shamt5;
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("slti")) {
			if (registers.get(rs1).getValue().longValue() < line.imm) {
				registers.get(rd).setValue(1);
			} else {
				registers.get(rd).setValue(0);
			}
		} else if (line.instruction.equals("sltiu")) {
			if (registers.get(rs1).getValue().compareTo(new BigInteger(Long.toString(line.imm))) == -1) {
				registers.get(rd).setValue(1);
			} else {
				registers.get(rd).setValue(0);
			}
		} else if (line.instruction.equals("lb")) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long_val = memory.read(long_val, 1, true, false);
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("lh")) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long_val = memory.read(long_val, 2, true, false);
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("lbu")) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long_val = memory.read(long_val, 1, true, false);
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("lhu")) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long_val = memory.read(long_val, 2, true, false);
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("lw")) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long_val = memory.read(long_val, 4, true, false);
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("ld")) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long_val = memory.read(long_val, 8, true, false);
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("addiw")) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			registers.get(rd).setValue(signExtend32To64(BigInteger.valueOf(long_val)));
		}
		modifyPC();
	}

	private void handleTypeS(Line line) throws PageFaultException, NoOfByteException, IRQException, AccessFaultException {
		String rs1 = Registers.getRegXNum32(line.rs1);
		String rs2 = Registers.getRegXNum32(line.rs2);

		if (line.instruction.equals("sb")) {
			long address = registers.get(rs2).getValue().longValue();
			memory.write(address + line.imm, registers.get(rs1).getValue().longValue(), 1, false, true);
		} else if (line.instruction.equals("sh")) {
			long address = registers.get(rs2).getValue().longValue();
			memory.write(address + line.imm, registers.get(rs1).getValue().longValue(), 2, false, true);
		} else if (line.instruction.equals("sw")) {
			long address = registers.get(rs2).getValue().longValue();
			memory.write(address + line.imm, registers.get(rs1).getValue().longValue(), 4, false, true);
		} else if (line.instruction.equals("sd")) {
			long address = registers.get(rs2).getValue().longValue();
			memory.write(address + line.imm, registers.get(rs1).getValue().longValue(), 8, false, true);
		}
		modifyPC();
	}

	private void handleTypeB(Line line) throws NoOfByteException, PageFaultException, IRQException {
		String rs1 = Registers.getRegXNum32(line.rs1);
		String rs2 = Registers.getRegXNum32(line.rs2);
		boolean modPC = false;

		if (line.instruction.equals("beq") || line.instruction.equals("beqz")) {
//			System.out.println(" " + registers.get(rs1).getValue().longValue() + " " + registers.get(rs2).getValue().longValue() + " " + line.imm);
			if (registers.get(rs1).getValue().longValue() == registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instruction.equals("bne")) {
			if (registers.get(rs1).getValue().longValue() != registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instruction.equals("bnez")) {
			if (registers.get(rs1).getValue().longValue() != 0) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instruction.equals("blt") || line.instruction.equals("bltz") || line.instruction.equals("bgtz")) {
			if (registers.get(rs1).getValue().longValue() < registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instruction.equals("bge") || line.instruction.equals("bgez") || line.instruction.equals("blez")) {
			if (registers.get(rs1).getValue().longValue() >= registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instruction.equals("bltu")) {
			if (registers.get(rs1).getValue().longValue() < registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instruction.equals("bgeu")) {
			if (registers.get(rs1).getValue().longValue() >= registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		}
		if (!modPC) {
			modifyPC();
		}
	}

	private void handleTypeR(Line line) throws PageFaultException, NoOfByteException, IRQException, AccessFaultException {
		String rs1 = Registers.getRegXNum32(line.rs1);
		String rs2 = Registers.getRegXNum32(line.rs2);
		String rd = Registers.getRegXNum32(line.rd);
		if (line.instruction.equals("sll")) {
			long val = registers.get(rs1).getValue().longValue() << registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("srl")) {
			long val = registers.get(rs1).getValue().longValue() >>> registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("sra")) {
			long val = registers.get(rs1).getValue().longValue() >> registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("add")) {
			long val = (registers.get(rs1).getValue().longValue() + registers.get(rs2).getValue().longValue()) & 0xffffffffL;
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("sub")) {
			long val;
//			if (registers.get(rs1).getValue().longValue() > registers.get(rs2).getValue().longValue()) {
			val = registers.get(rs1).getValue().longValue() - registers.get(rs2).getValue().longValue();
//			} else {
//				val = 0;
//			}
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("xor")) {
			long val = registers.get(rs1).getValue().longValue() ^ registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("or")) {
			long val = registers.get(rs1).getValue().longValue() | registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("and")) {
			long val = registers.get(rs1).getValue().longValue() & registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("slt")) {
			if (registers.get(rs1).getValue().longValue() < registers.get(rs2).getValue().longValue()) {
				registers.get(rd).setValue(1);
			} else {
				registers.get(rd).setValue(0);
			}
		} else if (line.instruction.equals("sltu")) {
			if (rs1.equals("zero")) { // instruction snez
				if (registers.get(rs2).getValue().longValue() == 0) {
					registers.get(rd).setValue(0);
				} else {
					registers.get(rd).setValue(1);
				}
			} else if (registers.get(rs1).getValue().longValue() < registers.get(rs2).getValue().longValue()) {
				registers.get(rd).setValue(1);
			} else {
				registers.get(rd).setValue(0);
			}
		} else if (line.instruction.equals("mul")) {
			long val = (registers.get(rs1).getValue().longValue() * registers.get(rs2).getValue().longValue()) & 0xffffffff;
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("mulh")) {
			long val = (registers.get(rs1).getValue().longValue() * registers.get(rs2).getValue().longValue()) & 0xffffffff00000000L;
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("mulhsu")) {
			long val = (registers.get(rs1).getValue().multiply(registers.get(rs2).getValue())).and(new BigInteger("0xffffffff00000000", 16)).longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("mulhu")) {
			long val = (registers.get(rs1).getValue().multiply(registers.get(rs2).getValue())).and(new BigInteger("0xffffffff00000000", 16)).longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("div")) {
			long val = registers.get(rs1).getValue().longValue() / registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("divu")) {
			long val = registers.get(rs1).getValue().divide(registers.get(rs2).getValue()).longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("rem")) {
			long val = registers.get(rs1).getValue().longValue() % registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("remu")) {
			long val = registers.get(rs1).getValue().mod(registers.get(rs2).getValue()).longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("lr.w")) { //32A Standard Extension

		} else if (line.instruction.equals("sc.w")) {

		} else if (line.instruction.equals("amoswap.w.aq")) {
			long long_val = registers.get(rs1).getValue().longValue();
			long val = registers.get(rs2).getValue().longValue();
			long_val = memory.read(long_val, 8, true, false);
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(long_val);
			}
			memory.write(registers.get(rs1).getValue().longValue(), val, 1, false, true);
		} else if (line.instruction.equals("amoswap.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			long rs2Value = registers.get(rs2).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
			}
			memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
		} else if (line.instruction.equals("amoadd.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = registers.get(rs2).getValue().longValue() + rs1Value;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amoxor.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = registers.get(rs2).getValue().longValue() ^ rs1Value;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amoand.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = registers.get(rs2).getValue().longValue() & rs1Value;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amoor.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = registers.get(rs2).getValue().longValue() | rs1Value;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amomin.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = (registers.get(rs2).getValue().longValue() >= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amomax.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = (registers.get(rs2).getValue().longValue() <= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amominu.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = (registers.get(rs2).getValue().longValue() >= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amomaxu.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = (registers.get(rs2).getValue().longValue() <= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("lr.d")) { //64A Standard Extension

		} else if (line.instruction.equals("sc.d")) {

		} else if (line.instruction.equals("amoswap.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			long rs2Value = registers.get(rs2).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amoadd.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = registers.get(rs2).getValue().longValue() + rs1Value;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amoxor.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = registers.get(rs2).getValue().longValue() ^ rs1Value;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amoand.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = registers.get(rs2).getValue().longValue() & rs1Value;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amoor.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = registers.get(rs2).getValue().longValue() | rs1Value;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amomin.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = (registers.get(rs2).getValue().longValue() >= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amomax.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = (registers.get(rs2).getValue().longValue() <= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amominu.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = (registers.get(rs2).getValue().longValue() >= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amomaxu.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = (registers.get(rs2).getValue().longValue() <= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("remuw")) {
			long rs1Value = registers.get(rs1).getValue().longValue() & 0xffffffffl;
			long rs2Value = registers.get(rs2).getValue().longValue() & 0xffffffffl;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value % rs2Value);
			}
		} else if (line.instruction.equals("muluw")) {
			long rs1Value = registers.get(rs1).getValue().longValue() & 0xffffffffl;
			long rs2Value = registers.get(rs2).getValue().longValue() & 0xffffffffl;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value * rs2Value);
			}
		} else if (line.instruction.equals("divuw")) {
			long rs1Value = registers.get(rs1).getValue().longValue() & 0xffffffffl;
			long rs2Value = registers.get(rs2).getValue().longValue() & 0xffffffffl;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value / rs2Value);
			}
		} else if (line.instruction.equals("subw")) {
			long val;
			val = registers.get(rs1).getValue().and(BigInteger.valueOf(0xffffffffl)).longValue() - registers.get(rs2).getValue().and(BigInteger.valueOf(0xffffffffl)).longValue();
			registers.get(rd).setValue(val);
			registers.get(rd).setValue(signExtend32To64(registers.get(rd).getValue()));
		} else {
			System.out.println("FUCK, no way to execute");
			System.out.println(line);
			System.exit(1000);
		}

		modifyPC();
	}

	private void handleTypeU(Line line) throws NoOfByteException, PageFaultException, IRQException {
		String rd = Registers.getRegXNum32(line.rd);
		if (line.instruction.equals("lui")) {
			long value = signExtend64(line.imm << 12, 31);
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("auipc")) {
			//registers.get(rd).value = registers.et("pc").value + value1;
			long value = registers.get("pc").getValue().longValue() + (line.imm << 12);//pc relative address
			value = value & 0xffffffffl;
			registers.get(rd).setValue(value);
		}

		modifyPC();
	}

	private void handleCityu(Line line) throws PageFaultException, NoOfByteException, IRQException {
		String rd = Registers.getRegXNum32(line.rd);
		String rs1 = Registers.getRegXNum32(line.rs1);

		long value;
		value = registers.get(rs1).getValue().longValue();
		value *= value;
		registers.get(rd).setValue(value);

		modifyPC();
	}

	private void handleEcallOrTrap(Line line) throws NoOfByteException, PageFaultException, IRQException {
		String rs1 = Registers.getRegXNum32(line.rs1);
		String rd = Registers.getRegXNum32(line.rd);
		String csr = Registers.getCsrNum12(line.csr);

		if (line.instruction.equals("ecall")) {
			registers.get("mepc").setValue(registers.get("pc").getValue().longValue());
			registers.get("pc").setValue(registers.get("mtvec").getValue().longValue() & 0xfffffffffffffffcL);
			registers.get("mcause").setValue(0xb); //machine external interrupt
		} else if (line.instruction.equals("ebreak")) {

		} else if (line.instruction.equals("wfi")) {
			int mie = (int) registers.get("mstatus").getValue().longValue() & 0b1000;
			int hie = (int) registers.get("mstatus").getValue().longValue() & 0b0100;
			int sie = (int) registers.get("mstatus").getValue().longValue() & 0b0010;
			int uie = (int) registers.get("mstatus").getValue().longValue() & 0b0001;

			if (mie == 8 || hie == 4 || sie == 2 || uie == 1) {
				registers.get("mepc").setValue(registers.get("pc").getValue().longValue() + 4);
			}
			modifyPC();

		} else if (line.instruction.equals("csrrw")) {
			/* The CSRRW (Atomic Read/Write CSR) instruction atomically swaps values in the CSRs and integer registers. 
			CSRRW reads the old value of the CSR, zero-extends the value to XLEN bits, then writes it to integer 
			register rd. The initial value in rs1 is written to the CSR. If rd=x0, then the instruction shall not 
			read the CSR and shall not cause any of the side effects that might occur on a CSR read. 
			A CSRRW with rs1=x0 will attempt to write zero to the destination CSR.*/
//			csr = CSR.csrToUse(csr);
/*			long csr_val = registers.get(csr).getValue().longValue();
			long rs1_val = registers.get(rs1).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(csr_val);
			}
			if (csr.equals("mideleg")) { //temporarily, should be wrong becuz too weird
				registers.get(csr).setValue((csr_val & ~CSR.MIDELEG_MASK) | (rs1_val & CSR.MIDELEG_MASK));
			} else {
				CSR.checkWritePermission(csr);
				registers.get(csr).setValue(rs1_val & CSR.getMask(csr));
				if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
					PMP.updatePmp(csr);
				}
			}
			modifyPC();
		} else if (line.instruction.equals("csrrs")) {
			/* The CSRRS (Atomic Read and Set Bits in CSR) instruction reads the value of the CSR, zero-extends 
			the value to XLEN bits, and writes it to integer register rd. The initial value in integer register 
			rs1 is treated as a bit mask that specifies bit positions to be set in the CSR. Any bit that is high 
			in rs1 will cause the corresponding bit to be set in the CSR, if that CSR bit is writable. Other bits 
			in the CSR are not explicitly written. 
			For both CSRRS and CSRRC, if rs1=x0, then the instruction will not write to the CSR at all, and so shall 
			not cause any of the side effects that might otherwise occur on a CSR write, nor raise illegal instruction 
			exceptions on accesses to read-only CSRs. Both CSRRS and CSRRC always read the addressed CSR and cause any 
			read side effects regardless of rs1 and rd fields. Note that if rs1 specifies a register holding a zero value 
			other than x0, the instruction will still attempt to write the unmodified value back to the CSR and will cause 
			any attendant side effects.
			 */
//			csr = CSR.csrToUse(csr);
/*			long csr_val = registers.get(csr).getValue().longValue();
			long rs1_val = registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(csr_val);
			if (!rs1.equals("zero")) {
				CSR.checkWritePermission(csr);
				registers.get(csr).setValue((csr_val | rs1_val) & CSR.getMask(csr));
				if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
					PMP.updatePmp(csr);
				}
			}
			modifyPC();

		} else if (line.instruction.equals("csrrc")) {
			/* The CSRRC (Atomic Read and Clear Bits in CSR) instruction reads the value of the CSR, zero-extends the value to XLEN
			bits, and writes it to integer register rd. The initial value in integer register rs1 is treated as a bit mask that specifies
			bit positions to be cleared in the CSR. Any bit that is high in rs1 will cause the corresponding bit to be cleared in the CSR,
			if that CSR bit is writable. Other bits in the CSR are not explicitly written. If rs1 is x0, check conditions in above CSRRS. */
//			csr = CSR.csrToUse(csr);
/* 			long csr_val = registers.get(csr).getValue().longValue(), rs1_val = registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(csr_val);
			if (!rs1.equals("zero")) {
				CSR.checkWritePermission(csr);
				registers.get(csr).setValue(csr_val & (~rs1_val) & CSR.getMask(csr));
				if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
					PMP.updatePmp(csr);
				}
			}
			modifyPC();
		} else if (line.instruction.equals("csrrwi")) {
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(registers.get(csr).getValue().longValue());
			}
			if (registers.get(csr).getName().equals("read_only")) {
				System.out.println("Error: Writing to a Read only Register");
			} else {
				registers.get(csr).setValue(line.imm);
			}
			if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
				PMP.updatePmp(csr);
			}
			modifyPC();
		} else if (line.instruction.equals("csrrsi")) {
			registers.get(rd).setValue(registers.get(csr).getValue().longValue());
			if (line.imm != 0) {
				if (registers.get(csr).getName().equals("read_only")) {
					System.out.println("Error: Writing to a Read only Register");
				} else {
					registers.get(csr).setValue(registers.get(csr).getValue().longValue() | line.imm);
				}
				if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
					PMP.updatePmp(csr);
				}
			}
			modifyPC();
		} else if (line.instruction.equals("csrrci")) {
			registers.get(rd).setValue(registers.get(csr).getValue().longValue());
			if (line.imm != 0) {
				if (registers.get(csr).getName().equals("read_only")) {
					System.out.println("Error: Writing to a Read only Register");
				} else {
					registers.get(csr).setValue(registers.get(csr).getValue().longValue() & (~line.imm));
				}
				if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
					PMP.updatePmp(csr);
				}
			}
			modifyPC();
		} else if (line.instruction.equals("mret")) {
			long mpp = CommonLib.getValue(registers.get("mstatus").getValue().longValue(), 11, 12);
			if (mpp == 0) {
				CPUState.setPriv(0);
			} else if (mpp == 1) {
				CPUState.setPriv(1);
			} else if (mpp == 2) {
				CPUState.setPriv(2);
			} else if (mpp == 3) {
				CPUState.setPriv(3);
			}
			registers.get("pc").setValue(registers.get("mepc").getValue().longValue());
			long mstatus = registers.get("mstatus").getValue().longValue();
			//0x808-->0x800 
			long mpie = CommonLib.getValue(registers.get("mstatus").getValue().longValue(), 7, 7);
			long mpie_if_0 = 0xfffffff7;
			long mpie_if_1 = 0x8;

			if (mpie == 1) {
				mstatus = mstatus | mpie_if_1;
			} else if (mpie == 0) {
				mstatus = mstatus & mpie_if_0;
			}
			//mpie-->1
			mstatus = mstatus | 0x00000080;
			//mpp-->prv_u(0);
			mstatus = mstatus & 0xffffffffffffE7ffl;
			//mpv-->0
			mstatus = mstatus & 0xffffff7fffffffffl;
			registers.get("mstatus").setValue(BigInteger.valueOf(mstatus));
		} else if (line.instruction.equals("uret")) {
			System.out.println("not support " + line.code);
			System.exit(123);
		} else if (line.instruction.equals("sret")) {
			long spp = CommonLib.getValue(registers.get("sstatus").getValue().longValue(), 8, 8);
			if (spp == 0) {
				CPUState.setPriv(CPUState.USER_MODE);
			} else if (spp == 1) {
				CPUState.setPriv(CPUState.SUPERVISOR_MODE);
			}
			registers.get("pc").setValue(registers.get("sepc").getValue().longValue());
//			long sstatus = registers.get("sstatus").value.longValue();
//			long spie = CommonLib.getValue(registers.get("sstatus").value.longValue(), 5, 5);
//			long sie = spie;
//			sie = sie & 0xfL;
//			sstatus = sstatus | sie;
//			spie = 0x20L;
//			sstatus = sstatus | spie;
//			registers.get("sstatus").setValue(BigInteger.valueOf(sstatus));

			long mstatus = registers.get("mstatus").getValue().longValue();
			mstatus = mstatus & 0xfffffffffffffeffl;
			mstatus = mstatus | 2;
			registers.get("mstatus").setValue(BigInteger.valueOf(mstatus));

			long sstatus = registers.get("sstatus").getValue().longValue();
			sstatus = sstatus & 0xfffffffffffffeffl;
			sstatus = sstatus | 2;
			registers.get("sstatus").setValue(BigInteger.valueOf(sstatus));

//          should change the mstatus too (havn't finish);
		} else if (line.instruction.equals("hret")) {
			System.out.println("not support " + line.code);
			System.exit(123);
		} else if (line.instruction.equals("sfence.vm")) {
			modifyPC();
		} else {
			logger.log(Level.SEVERE, "unhandle : " + line.code);
			return;
		}

	}

	private void handleCompressed(Line line) throws NoOfByteException, ArrayIndexOutOfBoundsException, PageFaultException, IRQException, AccessFaultException {
		boolean branched = false;

		if ((line.instruction.equals("c.lui") && line.rd == 2) || line.instruction.equals("c.addi16sp")) {
			if (line.imm != 0) {
				registers.get("sp").setValue(registers.get("sp").getValue().longValue() + line.imm);
			}
		} else if (line.instruction.equals("c.addi4spn")) {
//			if (line.imm != 0) {
			String rd = Registers.getRegXNum16(line.rd);
			long imm = line.imm;
			long value = registers.get("sp").getValue().longValue() + imm;
			registers.get(rd).setValue(value);
//			}

		} else if (line.instruction.equals("c.add")) {
			String rd = Registers.getRegXNum32(line.rd);
			String rs1 = Registers.getRegXNum32(line.rs1);
			long value = registers.get(rd).getValue().longValue() + registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.addi")) {
			String rd = Registers.getRegXNum32(line.rd);
			long imm = line.imm;
			long value = registers.get(rd).getValue().longValue() + imm;
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.and")) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long value = registers.get(rd).getValue().longValue() & registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.andi")) {
			String rd = Registers.getRegXNum16(line.rd);
			long imm = line.imm;
			long value = registers.get(rd).getValue().longValue() & imm;
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.or")) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long value = registers.get(rd).getValue().longValue() | registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.xor")) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long value = registers.get(rd).getValue().longValue() ^ registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.sub")) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long value = registers.get(rd).getValue().longValue() - registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.mv")) {
			String rd = Registers.getRegXNum32(line.rd);
			String rs1 = Registers.getRegXNum32(line.rs1);
			long value = registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.li")) {
			String rd = Registers.getRegXNum32(line.rd);
			long imm = line.imm;
			long value = registers.get("zero").getValue().longValue() + imm;
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.lui")) {
			String rd = Registers.getRegXNum32(line.rd);
			registers.get(rd).setValue(signExtend64(line.imm << 12, 31));
		} else if (line.instruction.equals("c.slli")) {
			String rd = Registers.getRegXNum32(line.rd);
			long imm = line.imm;
			long value = registers.get(rd).getValue().longValue() << imm;
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.srai")) {
			String rd = Registers.getRegXNum16(line.rd);
			long imm = line.imm;
			long value = registers.get(rd).getValue().longValue() >> imm;
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.srli")) {
			String rd = Registers.getRegXNum16(line.rd);
			long imm = line.imm;
			long value = registers.get(rd).getValue().longValue() >>> imm;
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.beqz")) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			if (registers.get(rs1).getValue().longValue() == 0) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				branched = true;
			}
		} else if (line.instruction.equals("c.bnez")) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			if (registers.get(rs1).getValue().longValue() != 0) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				branched = true;
			}
		} else if (line.instruction.equals("c.sw")) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			String rs2 = Registers.getRegXNum16(line.rs2);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
//			registers.get(rs1).value = (long) memory.get(value) & 0xffffffff;
			memory.write(value, registers.get(rs2).getValue().longValue(), 4, false, true);
		} else if (line.instruction.equals("c.swsp")) {
			String rs2 = Registers.getRegXNum32(line.rs2);
			long value = registers.get("sp").getValue().longValue() + line.imm;
			System.out.println("write" + Long.toHexString((registers.get(rs2).getValue().longValue() & 0xff)) + "to" + Long.toHexString(value));
//			registers.get(rs2).value = (long) memory.get(value) & 0xffffffff;  System.out.println("write" + Integer.toHexString((int) (registers.get(rs2).value & 0xff)) + "to" + Integer.toHexString(int_val));
			// System.out.println("write" + Integer.toHexString((int) (registers.get(rs2).value & 0xff00)) + "to" + Integer.toHexString(int_val));
			memory.write(value, registers.get(rs2).getValue().longValue(), 4, false, true);
		} else if (line.instruction.equals("c.fsw")) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			String rs2 = Registers.getRegFNum16(line.rs2);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
//			registers.get(rs1).value = (long) memory.get(value) & 0xffffffff;
			memory.write(value, registers.get(rs2).getValue().longValue(), 4, false, true);
//		} else if (line.instruction.equals("c.fswsp")) {
//			rs2 = Registers.getRegFNum32(line.rs2);
//			long_val = registers.get("sp").value.longValue() + ((int) line.imm);
////			registers.get(rs2).value = (long) memory.get(value) & 0xffffffff;
//			rd = Registers.getRegFNum32(line.rd);
//			if(rd.equals("ft1")){
//				memory.writeMemory(long_val, registers.get(rs2).getValue().longValue(), 4);			
//			}
		} else if (line.instruction.equals("c.fsd")) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			String rs2 = Registers.getRegFNum16(line.rs2);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
//			registers.get(rs1).value = (long) memory.get(value) & 0xffffffff;
			memory.write(value, registers.get(rs2).getValue().longValue(), 8, false, true);
		} else if (line.instruction.equals("c.sdsp")) {
			String rs2 = Registers.getRegXNum32(line.rs2);
			long value = registers.get("sp").getValue().longValue() + line.imm;
			memory.write(value, registers.get(rs2).getValue().longValue(), 8, false, true);
		} else if (line.instruction.equals("c.sd")) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			String rs2 = Registers.getRegXNum16(line.rs2);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
			memory.write(value, registers.get(rs2).getValue().longValue(), 8, false, true);
			if (value == 0x2004000L) {
				registers.get("mip").setValue(registers.get("mip").getValue().longValue() & 0xf);
			}
		} else if (line.instruction.equals("c.j")) {
			//registers.get().value = registers.get(rd).value >> imm;
			registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
			branched = true;
		} else if (line.instruction.equals("c.jr")) {
			String rd = Registers.getRegXNum32(line.rd);
			registers.get("pc").setValue(registers.get(rd).getValue().longValue());
			if (line.code.equals("sret")) {
				if (CPUState.priv == CPUState.SUPERVISOR_MODE) {
					long mstatus = registers.get("mstatus").getValue().longValue();
					mstatus = mstatus & 0xfffffeffl;
					mstatus = mstatus | 2;
					registers.get("mstatus").setValue(BigInteger.valueOf(mstatus));

					long sstatus = registers.get("sstatus").getValue().longValue();
					sstatus = sstatus & 0xfffffeffl;
					sstatus = sstatus | 2;
					registers.get("sstatus").setValue(BigInteger.valueOf(sstatus));
				}
			}
			branched = true;
		} else if (line.instruction.equals("c.lw")) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
			value = memory.read(value, 4, true, false);
			registers.get(rd).setValue(signExtend32To64(BigInteger.valueOf(value)));
		} else if (line.instruction.equals("c.lwsp")) {
			String rd = Registers.getRegXNum32(line.rd);
			long value = registers.get("sp").getValue().longValue() + line.imm;
			if (!rd.equals("zero") && (line.imm % 4 == 0)) {
				value = memory.read(value, 4, true, false);
				registers.get(rd).setValue(value);
			}
		} else if (line.instruction.equals("c.flw")) {
			String rd = Registers.getRegFNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
			if (line.imm % 4 == 0) {
				value = memory.read(value, 4, true, false);
				registers.get(rd).setValue(value);
			}

		} else if (line.instruction.equals("c.ld")) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
			value = memory.read(value, 8, true, false);
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.ldsp")) {
			String rd = Registers.getRegXNum32(line.rd);
			long value = registers.get("sp").getValue().longValue() + line.imm;
			value = memory.read(value, 8, true, false); //(memory.read(long_val + 3) & 0xff) << 24 | (memory.read(long_val + 2) & 0xff) << 16 | (memory.read(long_val + 1) & 0xff) << 8 | memory.read(long_val) & 0xff;
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.fld")) {
			String rd = Registers.getRegFNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
			if (line.imm % 8 == 0) {
				value = memory.read(value, 8, true, false);
				registers.get(rd).setValue(value);
			}
		} else if (line.instruction.equals("c.fldsp")) {
			String rd = Registers.getRegFNum32(line.rd);
			long value = registers.get("sp").getValue().longValue() + line.imm;

			if (line.imm % 8 == 0) {
				value = memory.read(value, 8, true, false);
				registers.get(rd).setValue(value);
			}
		} else if (line.instruction.equals("c.addiw")) {
			registers.get("pc").setValue(registers.get("pc").getValue().longValue() + 2);
			String rd = Registers.getRegXNum32(line.rd);
			long value = registers.get(rd).getValue().longValue() + line.imm;
			registers.get(rd).setValue(value);
			branched = true;
		} else if (line.instruction.equals("c.jalr")) {
			registers.get("ra").setValue(registers.get("pc").getValue().longValue() + 2);
			registers.get("pc").setValue(registers.get("rs1").getValue().longValue() + line.imm);
			branched = true;
		} else if (line.instruction.equals("c.ebreak")) {
		} else if (line.instruction.equals("c.subw")) {
			String rs2 = Registers.getRegXNum32(line.rs2);
			String rd = Registers.getRegXNum32(line.rd);
			long val = registers.get(rd).getValue().longValue() - registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else {
			System.err.println("unhandled instruction " + line.instruction);
			System.exit(3);
		}

		if (!branched) {
			modifyPC();
		}
	}

	private void handleTypeV(int arr[], Line line) throws ArrayIndexOutOfBoundsException, PageFaultException, NoOfByteException, IRQException {
		String rd = Registers.getRegXNum32(line.rd);
		String rs1 = Registers.getRegXNum32(line.rs1);
		String rs2 = Registers.getRegXNum32(line.rs2);
		String vd = Registers.getVregNum32(line.vd);
		String vs1 = Registers.getVregNum32(line.vs1);
		String vs2 = Registers.getVregNum32(line.vs2);
		String vs3 = Registers.getVregNum32(line.vs3);
		String vma = Registers.getVregNum32(line.vma);
		String vta = Registers.getVregNum32(line.vta);
		String vsew = Registers.getVregNum32(line.vsew);
		String vlmul = Registers.getVregNum32(line.vlmul);
		int imm = (arr[3] << 4) | ((arr[2] & 0xf0) >> 4);
		int shamt = (arr[3] & 0x01) << 4 | (arr[2] & 0xf0) >> 4;
		long long_val;
		int int_val;
		if (line.instruction.equals("vadd.vv")) {
//            System.out.println("vs1 = " + vs1);
//            System.out.println(vs1 + " = " + registers.get(vs1).getValue());
//            System.out.println("vs2 = " + vs2);
//            System.out.println(vs2 + " = " + registers.get(vs2).getValue());
//            System.out.println("vd = " + vd);
//            System.out.println(vd + " = " + registers.get(vd).getValue());
			long_val = registers.get(vs2).getValue().longValue() + registers.get(vs1).getValue().longValue();
			registers.get(vd).setValue(long_val);
		} else if (line.instruction.equals("vle32.v")) {

		}
		modifyPC();
	} */
}
