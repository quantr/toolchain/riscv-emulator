/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hk.quantr.riscv.translator;

import static hk.quantr.riscv.translator.Translator.mvR;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 *
 * @author Stupid Genius
 */
public class Stack {
	
	public static void pushReg(int x) {
		mvR.visitFieldInsn(Opcodes.GETSTATIC, "hk/quantr/riscv/emulator/Emulator", "registers", "[J");
		pushValue(mvR, x);
	}
	
	/* Push imm onto stack, provided sign extension.
		eg: To sign-extend a 12-bit imm, numberOfBits = 12, singExtension = true
	*/
	public static void pushImm(long imm, int numberOfBits, boolean signExtension) {
		if (signExtension &&((imm >> (numberOfBits-1)) & 1) == 1) imm = (-(1 << numberOfBits)) | imm;
		pushValue(mvR, imm);
	}
	
	public static void pushValue(MethodVisitor mv, long value) {
		if (value == 0) {
			mv.visitInsn(Opcodes.LCONST_0);
		} else if (value == 1) {
			mv.visitInsn(Opcodes.LCONST_1);
		} else {
			mv.visitLdcInsn(value);
		}
	}
	
	public static void pushValue(MethodVisitor mv, int value) {
		switch (value) { //iload_[m1-5], for x0 - x5, bipush [6 - 31], for x6 - x31
			case 0 -> mv.visitInsn(Opcodes.ICONST_0);
			case 1 -> mv.visitInsn(Opcodes.ICONST_1);
			case 2 -> mv.visitInsn(Opcodes.ICONST_2);
			case 3 -> mv.visitInsn(Opcodes.ICONST_3);
			case 4 -> mv.visitInsn(Opcodes.ICONST_4);
			case 5 -> mv.visitInsn(Opcodes.ICONST_5);
			default -> mv.visitVarInsn(Opcodes.BIPUSH, value);
		}
	}
}
