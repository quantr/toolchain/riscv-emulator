package hk.quantr.riscv.translator;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/*
Structure now: 

constructor -> initialize "registers" "memory"
initEmulator -> initialize preset values, write memory
runKernel -> run the whole kernel

useful sources:
wiki useful bytecode ins:https://en.wikipedia.org/wiki/List_of_Java_bytecode_instructions
our excel: https://docs.google.com/spreadsheets/d/1cXr2eCDKzaXIbukSNdxMMdwHIjUPNZeNrIcC9c0NLzE/edit#gid=1377447846
instrumentation: https://github.com/eugenp/tutorials/blob/master/asm/src/main/java/com/baeldung/examples/asm/instrumentation/Premain.java
				 http://www.egtry.com/java/bytecode/instrument
 */
public class Translator {

	ClassWriter cw;
	static MethodVisitor mvC, mvR; //mvC for constructor, mv for runKernel()

	//The start of everything...
	public void start() {
		//Class
		cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
		cw.visit(Opcodes.V1_8, Opcodes.ACC_PUBLIC, "Emulator", null, "java/lang/Object", null);
		//Functions
		mvC = cw.visitMethod(Opcodes.ACC_PUBLIC, "<init>", "()V", null, null);
		mvR = cw.visitMethod(Opcodes.ACC_PUBLIC, "runKernel", "()V", null, null);
		mvC.visitCode();
		mvR.visitCode();
		constructor();
//		example:
//		runExample();
	}

	//Finished translating, return bytes...
	public byte[] translate() {
		endFunction(mvC);
		endFunction(mvR);
		cw.visitEnd();
		return cw.toByteArray();
	}

	// Ending Scripts of a function
	public void endFunction(MethodVisitor mv) {
		mv.visitInsn(Opcodes.RETURN);
		mv.visitMaxs(-1, -1);
		mv.visitEnd();
	}

	public void runExample() {
		Instruction.addi(2, 0, 0x123);
		Instruction.addi(3, 2, 0x123);

		mvR.visitFieldInsn(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
		Stack.pushReg(3);
		mvR.visitInsn(Opcodes.LALOAD);
		mvR.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "println", "(J)V", false);
		
		mvR.visitFieldInsn(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
		mvR.visitFieldInsn(Opcodes.GETSTATIC, "hk/quantr/riscv/emulator/Memory", "mem80000000", "[B");
		mvR.visitInsn(Opcodes.ICONST_0);
		mvR.visitInsn(Opcodes.BALOAD);
		mvR.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "println", "(I)V", false);
	}

	//setup registers, memory fields...
	public void constructor() {
		mvC.visitVarInsn(Opcodes.ALOAD, 0);
		mvC.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
		mvC.visitInsn(Opcodes.RETURN);
	}
}
