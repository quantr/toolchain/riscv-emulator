package hk.quantr.riscv.translator;

import static hk.quantr.riscv.translator.Translator.mvR;
import org.objectweb.asm.Opcodes;

/**
 *
 * @author Stupid Genius
 */
public class Instruction {
	
	// Instructions
	public static void add(int rd, int rs1, int rs2) {
		Stack.pushReg(rd); //0 -> register, 1 -> rd_index
		Stack.pushReg(rs1); //2 -> register, 3 -> rs1_index
		mvR.visitInsn(Opcodes.LALOAD); //wipe out 2, 3 and 2 becomes value of rs1
		Stack.pushReg(rs2); //3 -> register, 4 -> rs2_index
		mvR.visitInsn(Opcodes.LALOAD); //wipe out 3, 4 and 3 becomes value of rs2
		mvR.visitInsn(Opcodes.LADD); //wipe out 2 and 3, 2 becomes value of result
		mvR.visitInsn(Opcodes.LASTORE); //wipe out 0, 1, and value is stored to rd
	}
	
	public static void addi(int rd, int rs1, long imm) {
		Stack.pushReg(rd);
		Stack.pushReg(rs1);
		mvR.visitInsn(Opcodes.LALOAD);
		Stack.pushImm(imm, 12, true);
		mvR.visitInsn(Opcodes.LADD);
		mvR.visitInsn(Opcodes.LASTORE);
	}

	public static void sub(int rd, int rs1, int rs2) {
		Stack.pushReg(rd);
		Stack.pushReg(rs1);
		mvR.visitInsn(Opcodes.LALOAD);
		Stack.pushReg(rs2);
		mvR.visitInsn(Opcodes.LALOAD);
		mvR.visitInsn(Opcodes.LSUB);
		mvR.visitInsn(Opcodes.LASTORE);
	}

	//Library
	
	
}
