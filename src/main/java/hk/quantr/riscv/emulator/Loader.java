/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hk.quantr.riscv.emulator;

public class Loader extends ClassLoader {
	static { registerAsParallelCapable(); }
	public static final Loader INSTANCE = new Loader();
	public Class<?> defineClass(String binaryName, byte[] bytecode) {
		return defineClass(binaryName, bytecode, 0, bytecode.length);
	}
}
