package hk.quantr.riscv.emulator;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.LongConverter;
import com.thoughtworks.xstream.security.AnyTypePermission;
import hk.quantr.riscv.cpu.CSRRegister;
import hk.quantr.riscv.cpu.Memory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashSet;

public class Setting {
	
	public HashSet<CSRRegister> registers = new HashSet<>();
	public HashSet<Memory> memory = new HashSet<>();
	
	public Setting(String filename) {
		try {
			XStream xstream = new XStream();
			xstream.addPermission(AnyTypePermission.ANY);
			xstream.autodetectAnnotations(true);
			xstream.registerConverter(new LongConverter());
			xstream.alias("Setting", Setting.class);
			xstream.alias("Register", CSRRegister.class);
			xstream.alias("Memory", Memory.class);
			xstream.fromXML(new FileInputStream(new File(filename)));
			
		} catch(FileNotFoundException ex) {
			System.out.println("Default Settings File is missing.");
		}
	}
}