package hk.quantr.riscv.emulator;

import hk.quantr.riscv.cpu.Memory;
import static hk.quantr.assembler.Assembler.allTokens;
import hk.quantr.assembler.AssemblerLib;
import hk.quantr.assembler.RISCVDisassembler;
import hk.quantr.assembler.RISCVDisassembler.DecodeType;
import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
import hk.quantr.assembler.antlr.RISCVAssemblerParser;
import hk.quantr.assembler.exception.NoOfByteException;
import hk.quantr.assembler.exception.WrongInstructionException;
import hk.quantr.assembler.print.MessageHandler;
import hk.quantr.assembler.riscv.il.DisasmStructure;
import hk.quantr.assembler.riscv.il.Line;
import hk.quantr.assembler.riscv.listener.RISCVErrorListener;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.dwarf.DwarfLib;
import hk.quantr.dwarf.elf.Elf_Phdr;
import hk.quantr.javalib.CommonLib;
import hk.quantr.javalib.PropertyUtil;
import hk.quantr.riscv.cpu.CSRRegister;
import hk.quantr.riscv.cpu.GeneralRegister;
import hk.quantr.riscv.cpu.Register;
import hk.quantr.riscv.exception.AccessFaultException;
import hk.quantr.riscv.translator.Translator;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;
import java.util.TreeMap;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Token;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.io.FilenameUtils;

public final class Emulator {
	
	public Memory memory = new Memory();
	public RISCVDisassembler disassembler = new RISCVDisassembler();
	public TreeMap<Long, Line> instructions = new TreeMap<>();
	public static LinkedHashMap<String, Register> registers = new LinkedHashMap<>();
	private ArrayList<Dwarf> dwarfArrayList;
	CommandLine cmd;
	boolean isXml = false;
	String dumpJson;
	
	Translator t;
	Loader l;
	Object instance;
	
	public void start_emulation() throws Exception {
//		byte[] b = t.translate();
//		Class c = l.defineClass("Emulator", b);
//		instance = c.getDeclaredConstructor().newInstance();
//		//run
//		c.getMethod("runKernel").invoke(instance);
		String prevCommand = "";
		int x_n = 1;
		char x_u = 'b', x_f = 'x';
		Scanner in = new Scanner(System.in);
		outer:
		while (true) {
			System.out.print(">");
			String commands[] = cmd.hasOption("commands") ? cmd.getOptionValue("commands").split(";") : in.nextLine().trim().split(";");
			try {
				for (String command : commands) {
					if (command.equals("q")) {
						break outer;
					} else if (!command.isBlank()) {
						prevCommand = command;
					} else {
						command = prevCommand;
					}
					command = command.replaceAll("( )+", " ");
					switch(command) { //single word commands first
						case "v" -> {}
						case "priv" -> {}
						case "c", "cont" -> {}
						case "p", "pause" -> {}
						case "memoryhistory" ->{}
						case "time" ->{}
						case "cputick" ->{}
						case "help" ->{}
						default ->{
						
						
						}
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public Emulator(String args[]) {

		//options
		Options options = new Options();
		options.addOption("v", "version", false, "display version");
		options.addOption("h", "help", false, "help");
		options.addOption(Option.builder("d").required(false).hasArg().argName("json").desc("log execution into json file").longOpt("dump").build());
		options.addOption(Option.builder("f").required(false).hasArg().argName("file").desc("file system image").longOpt("file").build());
		options.addOption(Option.builder("h2").required(false).hasArg().argName("h2 db file").desc("log execution into h2 database").build());
		options.addOption(Option.builder("c").required(false).hasArg().argName("commands").desc("specify your commands by \"a;b;c\"").longOpt("commands").build());

		if (Arrays.asList(args).contains("-h") || Arrays.asList(args).contains("--help")) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("java -jar emulator-xx.jar [OPTIONS] <filename>", options);
			return;
		} else if (Arrays.asList(args).contains("-v") || Arrays.asList(args).contains("--version")) {
			System.out.println("version : " + PropertyUtil.getProperty("main.properties", "version"));
			return;
		}
		try {
			System.out.println("Initializing Emulator");
			CommandLineParser cliParser = new DefaultParser();
			cmd = cliParser.parse(options, args);
			List<String> arguments = cmd.getArgList();
			if (arguments.isEmpty()) {
				System.out.println("Please specify input file.");
				System.exit(1);
			}
			if (!new File(arguments.get(0)).exists() || !new File(arguments.get(0)).isFile()) {
				System.out.println(arguments.get(0) + " not found.");
				System.exit(2);
			}

			l = new Loader();
			t = new Translator();
			t.start();

			/* File Handling
				.elf or no extension == elf; .o == riscv bytecode; .s == riscv assembly .bin = binary
			 */
			String fileExtension = FilenameUtils.getExtension(arguments.get(0));
			File inputFile = new File(arguments.get(0));
			switch (fileExtension) {
				case "s" -> {
					String content = AssemblerLib.preProcess(inputFile, "rv64");
					RISCVErrorListener errorListener = new RISCVErrorListener(content);
					RISCVAssemblerLexer lexer = new RISCVAssemblerLexer(CharStreams.fromString(content));
					allTokens.clear();
					for (Token token : lexer.getAllTokens()) {
						allTokens.add(token);
					}
					lexer.reset();
					CommonTokenStream tokenStream = new CommonTokenStream(lexer);
					RISCVAssemblerParser parser = new RISCVAssemblerParser(tokenStream);
					parser.encoder.arch = "rv64";
					parser.removeErrorListeners();
					parser.addErrorListener(errorListener);
					parser.assemble();

					/* byte bytes[] = parser.encoder.out.toByteArray();
					RISCVDisassembler disassembler = new RISCVDisassembler(false);
					DisasmStructure disasmStructure = disassembler.disasm(new ByteArrayInputStream(bytes), 0); */
					/*for (hk.quantr.assembler.riscv.il.Line line : disasmStructure.lines) {
						MessageHandler.println(line.toString());
					}*/
				}
				case "o" -> {
					try {
						byte bytes[] = Files.readAllBytes(Paths.get(arguments.get(0)));
						RISCVDisassembler disassembler = new RISCVDisassembler(false);
						DisasmStructure disasmStructure = disassembler.disasm(new ByteArrayInputStream(bytes), 0);
						for (hk.quantr.assembler.riscv.il.Line line : disasmStructure.lines) {
							MessageHandler.println(line.toString());
						}
					} catch (IOException | RecognitionException | NoOfByteException | WrongInstructionException ex) {
						MessageHandler.errorPrintln(ex);
						MessageHandler.flushXML();
						System.exit(4);
					}
				}
				default -> {
					FileInputStream fis = new FileInputStream(inputFile);
					byte buffer[] = new byte[7];
					fis.read(buffer, 0, 7);
					byte elfMagic[] = new byte[]{127, 69, 76, 70, 2, 1, 1};

					if (Arrays.equals(buffer, elfMagic)) {
						dwarfArrayList = DwarfLib.init(inputFile, 0, false);
						RandomAccessFile rf = new RandomAccessFile(inputFile, "r");
						int[] ins = new int[4];
						for (Dwarf dwarf : dwarfArrayList) {
							for (Elf_Phdr programHeader : dwarf.programHeaders) {
								if (programHeader.getP_type() == 1) {
									rf.seek(programHeader.getP_offset().longValue());
									int i = 0;
									int noOfByte = -1;
									for (int x = 0; x < programHeader.getP_filesz().longValue(); x++) {
										long address = programHeader.getP_paddr().longValue() + x;
										byte b = rf.readByte();
										memory.write(address, b);
										try {
											//pre-decode all instructions first
											if (i == 0) ins = new int[(noOfByte = RISCVDisassembler.getNoOfByte(b & 0x7f))];
											ins[i] = b & 0xff; i++;
											if (i == noOfByte) {
												decodeIns(address+1-i, ins); 
												i = 0;
											}
										} catch (NoOfByteException ex) {} //must catch. If wrong opcodes, skip
									}
//									for (Map.Entry<Long, Line> entry: instructions.entrySet()) { //try it yourself
//										System.out.println(entry.getValue().code);
//									}
								}
							}
						}
					} else {
						MessageHandler.println("Unsupported file format!");
						MessageHandler.flushXML();
						System.exit(4);
					}
				}
			}
			initEmulator();
			start_emulation();
		} catch (Exception ex) {
			System.out.println(ex);
		}
	}

	public void decodeIns(long address, int[] ins) {
		DecodeType type = RISCVDisassembler.getDecodeType(ins[0] & 0x7f);
		try {
			switch (type) {
				case DecodeType.decodeTypeB -> { instructions.put(address, disassembler.decodeTypeB(ins)); }
				case "decodeTypeI" -> { instructions.put(address, disassembler.decodeTypeI(ins)); }
				case "decodeTypeS" -> { instructions.put(address, disassembler.decodeTypeS(ins)); }
				case "decodeTypeFS" -> { instructions.put(address, disassembler.decodeTypeFS(ins)); }
				case "decodeTypeFR" -> { instructions.put(address, disassembler.decodeTypeFR(ins)); }
				case "decodeTypeR" -> { instructions.put(address, disassembler.decodeTypeR(ins)); }
				case "decodeTypeR4" -> { instructions.put(address, disassembler.decodeTypeR4(ins)); }
				case "decodeTypeU" -> { instructions.put(address, disassembler.decodeTypeU(ins)); }
				case "ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU" -> { instructions.put(address, disassembler.ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU(ins)); }
				case "jal" -> { instructions.put(address, disassembler.jal(ins)); }
				case "jalr" -> { instructions.put(address, disassembler.jalr(ins)); }
				case "fence" -> { instructions.put(address, disassembler.fence(ins)); }
				case "magic" -> { instructions.put(address, disassembler.decodeMagic(ins)); }
				case "compressed" -> { instructions.put(address, disassembler.decodeCompressed(ins)); }
				case "decodeTypeV" -> { instructions.put(address, disassembler.decodeTypeV(ins)); }
				default -> { throw new WrongInstructionException("No such instruction!"); }
			}
		} catch (WrongInstructionException ex) {} //must catch.
	}
	
	public void initEmulator() {
		//Init Registers
		registers.put("pc", new GeneralRegister("pc", "normal", 0x1000));
		for (int x = 0; x <= 31; x++) {
			registers.put("x" + x, new GeneralRegister("x" + x, "normal", 0x0));
		}
		registers.put("zero", registers.get("x0"));
		registers.put("ra", registers.get("x1"));
		registers.put("sp", registers.get("x2"));
		registers.put("gp", registers.get("x3"));
		registers.put("tp", registers.get("x4"));
		registers.put("t0", registers.get("x5"));
		registers.put("t1", registers.get("x6"));
		registers.put("t2", registers.get("x7"));
		registers.put("s0", registers.get("x8"));
		registers.put("s1", registers.get("x9"));
		registers.put("a0", registers.get("x10"));
		registers.put("a1", registers.get("x11"));
		registers.put("a2", registers.get("x12"));
		registers.put("a3", registers.get("x13"));
		registers.put("a4", registers.get("x14"));
		registers.put("a5", registers.get("x15"));
		registers.put("a6", registers.get("x16"));
		registers.put("a7", registers.get("x17"));
		registers.put("s2", registers.get("x18"));
		registers.put("s3", registers.get("x19"));
		registers.put("s4", registers.get("x20"));
		registers.put("s5", registers.get("x21"));
		registers.put("s6", registers.get("x22"));
		registers.put("s7", registers.get("x23"));
		registers.put("s8", registers.get("x24"));
		registers.put("s9", registers.get("x25"));
		registers.put("s10", registers.get("x26"));
		registers.put("s11", registers.get("x27"));
		registers.put("t3", registers.get("x28"));
		registers.put("t4", registers.get("x29"));
		registers.put("t5", registers.get("x30"));
		registers.put("t6", registers.get("x31"));
		
		//Floating point registers
		for (int x = 0; x <= 31; x++) {
			registers.put("f" + x, new GeneralRegister("f" + x, "floating", 0x0));
		}
		for (int x = 0; x <= 7; x++) {
			registers.put("ft" + x, registers.get("f" + x));
		}
		for (int x = 0; x <= 1; x++) {
			registers.put("fs" + x, registers.get("f" + (x + 8)));
		}
		for (int x = 0; x <= 7; x++) {
			registers.put("fa" + x, registers.get("f" + (x + 10)));
		}
		for (int x = 2; x <= 11; x++) {
			registers.put("fs" + x, registers.get("f" + (x + 16)));
		}
		for (int x = 8; x <= 11; x++) {
			registers.put("ft" + x, registers.get("f" + (x + 20)));
		}
		
		//Vector registers
		for (int x = 0; x <= 31; x++) {
			registers.put("v" + x, new GeneralRegister("v" + x, "vector", 0x0));
		}

		//VCSR
		registers.put("vtype", new GeneralRegister("vtype", "vector", 0x0));
		registers.put("vsew", new GeneralRegister("vsew", "vector", 0x0));
		registers.put("vlmul", new GeneralRegister("vlmul", "vector", 0x0));
		registers.put("vta", new GeneralRegister("vta", "vector", 0x0));
		registers.put("vma", new GeneralRegister("vma", "vector", 0x0));
		registers.put("vl", new GeneralRegister("vl", "vector", 0x0));
		registers.put("vlenb", new GeneralRegister("vlenb", "vector", 0x0));
		registers.put("vstart", new GeneralRegister("vstart", "vector", 0x0));
		registers.put("vxrm", new GeneralRegister("vxrm", "vector", 0x0));
		registers.put("vxsat", new GeneralRegister("vxsat", "vector", 0x0));
		registers.put("vcsr", new GeneralRegister("vcsr", "vector", 0x0));
		
		//CSR Registers. doc from https://five-embeddev.com/riscv-isa-manual/latest/priv-csrs.html#csrrwpriv
		registers.put("ustatus", new CSRRegister("ustatus", 0x000, 0x0));
		registers.put("uie", new CSRRegister("uie", 0x004, 0x0));
		registers.put("utvec", new CSRRegister("utvec", 0x005, 0x0));
		registers.put("uscratch", new CSRRegister("uscratch", 0x040, 0x0));
		registers.put("uepc", new CSRRegister("uepc", 0x041, 0x0));
		registers.put("ucause", new CSRRegister("ucause", 0x042, 0x0));
		registers.put("utval", new CSRRegister("utval", 0x043, 0x0));
		registers.put("uip", new CSRRegister("uip", 0x044, 0x0));
		registers.put("fflags", new CSRRegister("fflags", 0x001, 0x0));
		registers.put("frm", new CSRRegister("frm", 0x002, 0x0));
		registers.put("fcsr", new CSRRegister("fcsr", 0x003, 0x0));
		registers.put("cycle", new CSRRegister("cycle", 0xC00, 0x0));
		registers.put("time", new CSRRegister("time", 0xC01, 0x0));
		registers.put("instret", new CSRRegister("instret", 0xC02, 0x0));
		for (int x = 3; x <= 31; x++) {
			registers.put("hpmcounter" + x, new CSRRegister("hpmcounter" + x, 0xC00 + x, 0x0));
		}
		registers.put("cycleh", new CSRRegister("cycleh", 0xC80, 0x0));
		registers.put("timeh", new CSRRegister("timeh", 0xC81, 0x0));
		registers.put("instreth", new CSRRegister("instreth", 0xC82, 0x0));
		for (int x = 3; x <= 31; x++) {
			registers.put("hpmcounter" + x + "h", new CSRRegister("hpmcounter" + x + "h", 0xC80 + x, 0x0));
		}
		registers.put("sstatus", new CSRRegister("sstatus", 0x100, 0x0));
		registers.put("sedeleg", new CSRRegister("sedeleg", 0x102, 0x0));
		registers.put("sideleg", new CSRRegister("sideleg", 0x103, 0x0));
		registers.put("sie", new CSRRegister("sie", 0x104, 0x0));
		registers.put("stvec", new CSRRegister("stvec", 0x105, 0x0));
		registers.put("scounteren", new CSRRegister("scounteren", 0x106, 0x0));
		registers.put("sscratch", new CSRRegister("sscratch", 0x140, 0x0));
		registers.put("sepc", new CSRRegister("sepc", 0x141, 0x0));
		registers.put("scause", new CSRRegister("scause", 0x142, 0x0));
		registers.put("stval", new CSRRegister("stval", 0x143, 0x0));
		registers.put("sip", new CSRRegister("sip", 0x144, 0x0));
		registers.put("satp", new CSRRegister("satp", 0x180, 0x0));
		registers.put("scontext", new CSRRegister("scontext", 0x5A8, 0x0));
		registers.put("hstatus", new CSRRegister("hstatus", 0x600, 0x0));
		registers.put("hedeleg", new CSRRegister("hedeleg", 0x602, 0x0));
		registers.put("hideleg", new CSRRegister("hideleg", 0x603, 0x0));
		registers.put("hie", new CSRRegister("hie", 0x604, 0x0));
		registers.put("hcounteren", new CSRRegister("hcounteren", 0x606, 0x0));
		registers.put("hgeie", new CSRRegister("hgeie", 0x607, 0x0));
		registers.put("htval", new CSRRegister("htval", 0x643, 0x0));
		registers.put("hip", new CSRRegister("hip", 0x644, 0x0));
		registers.put("hvip", new CSRRegister("hvip", 0x645, 0x0));
		registers.put("htinst", new CSRRegister("htinst", 0x64A, 0x0));
		registers.put("hgeip", new CSRRegister("hgeip", 0xE12, 0x0));
		registers.put("hgatp", new CSRRegister("hgatp", 0x680, 0x0));
		registers.put("hcontext", new CSRRegister("hcontext", 0x6A8, 0x0));
		registers.put("htimedelta", new CSRRegister("htimedelta", 0x605, 0x0));
		registers.put("htimedeltah", new CSRRegister("htimedeltah", 0x615, 0x0));
		registers.put("vsstatus", new CSRRegister("vsstatus", 0x200, 0x0));
		registers.put("vsie", new CSRRegister("vsie", 0x204, 0x0));
		registers.put("vstvec", new CSRRegister("vstvec", 0x205, 0x0));
		registers.put("vsscratch", new CSRRegister("vsscratch", 0x240, 0x0));
		registers.put("vsepc", new CSRRegister("vsepc", 0x241, 0x0));
		registers.put("vscause", new CSRRegister("vscause", 0x242, 0x0));
		registers.put("vstval", new CSRRegister("vstval", 0x243, 0x0));
		registers.put("vsip", new CSRRegister("vsip", 0x244, 0x0));
		registers.put("vsatp", new CSRRegister("vsatp", 0x280, 0x0));
		registers.put("mvendorid", new CSRRegister("mvendorid", 0xF11, 0x0)); //fixed id for hart 
		registers.put("marchid", new CSRRegister("marchid", 0xF12, 0x0)); // fixed id for hart
		registers.put("mimpid", new CSRRegister("mimpid", 0xF13, 0x0));// fixed value 
		registers.put("mhartid", new CSRRegister("mhartid", 0xF14, 0x0));
		registers.put("mstatus", new CSRRegister("mstatus", 0x300, 0x0)); // machine mode
		registers.put("misa", new CSRRegister("misa", 0x301, 0x0));
		registers.put("medeleg", new CSRRegister("medeleg", 0x302, 0x0));
		registers.put("mideleg", new CSRRegister("mideleg", 0x303, 0x0));
		registers.put("mie", new CSRRegister("mie", 0x304, 0x0));
		registers.put("mtvec", new CSRRegister("mtvec", 0x305, 0x0));
		registers.put("mcounteren", new CSRRegister("mcounteren", 0x306, 0x0));
		registers.put("mstatush", new CSRRegister("mstatush", 0x310, 0x0));
		registers.put("mscratch", new CSRRegister("mscratch", 0x340, 0x0));
		registers.put("mepc", new CSRRegister("mepc", 0x341, 0x0));
		registers.put("mcause", new CSRRegister("mcause", 0x342, 0x0));
		registers.put("mtval", new CSRRegister("mtval", 0x343, 0x0));
		registers.put("mip", new CSRRegister("mip", 0x344, 0x0));
		registers.put("mtinst", new CSRRegister("mtinst", 0x34A, 0x0));
		registers.put("mtval2", new CSRRegister("mtval2", 0x34B, 0x0));
		for (int x = 0; x <= 3; x++) {
			registers.put("pmpcfg" + x, new CSRRegister("pmpcfg" + x, 0x3A0 + x, 0x0));
		}
		for (int x = 0; x <= 15; x++) {
			registers.put("pmpaddr" + x, new CSRRegister("pmpaddr" + x, 0x3B0 + x, 0x0));
		}
		registers.put("mcycle", new CSRRegister("mcycle", 0xB00, 0x0));
		registers.put("minstret", new CSRRegister("minstret", 0xB02, 0x0));
		for (int x = 3; x <= 31; x++) {
			registers.put("mhpmcounter" + x, new CSRRegister("mhpmcounter" + x, 0xB00 + x, 0x0));
		}
		registers.put("mcycleh", new CSRRegister("mcycleh", 0xB80, 0x0));
		registers.put("minstreth", new CSRRegister("minstreth", 0xB82, 0x0));
		for (int x = 3; x <= 31; x++) {
			registers.put("mhpmcounter" + x + "h", new CSRRegister("mhpmcounter" + x + "h", 0xB80 + x, 0x0));
		}
		registers.put("mcountinhibit", new CSRRegister("mcountinhibit", 0x320, 0x0));
		for (int x = 3; x <= 31; x++) {
			registers.put("mhpmevent" + x, new CSRRegister("mhpmevent" + x, 0x320 + x, 0x0));
		}
		registers.put("tselect", new CSRRegister("tselect", 0x7A0, 0x0));
		registers.put("tdata1", new CSRRegister("tdata1", 0x7A1, 0x0));
		registers.put("tdata2", new CSRRegister("tdata2", 0x7A2, 0x0));
		registers.put("tdata3", new CSRRegister("tdata3", 0x7A3, 0x0));
		registers.put("mcontext", new CSRRegister("mcontext", 0x7A8, 0x0));
		registers.put("dcsr", new CSRRegister("dcsr", 0x7B0, 0x0));
		registers.put("dpc", new CSRRegister("dpc", 0x7B1, 0x0));
		registers.put("dscratch0", new CSRRegister("dscratch0", 0x7B2, 0x0));
		registers.put("dscratch1", new CSRRegister("dscratch1", 0x7B3, 0x0));
		
		//Default settings
		Setting settings = new Setting("defaultSettings.xml");
		for (CSRRegister reg : settings.registers) {
			registers.get(reg.name).setValue(reg.value);
		}
		for (Memory mem : settings.memory) {
			String arr[] = mem.value.split(",");
			int address = 0x1000;
			for (String s : arr) {
				byte[] bytes = CommonLib.string2bytes(s.trim());
				for (byte b: bytes) {
					try {memory.write(address, b);} catch (AccessFaultException ex) {}
					address++;
				}
			}
		}
	}

	public static void main(String[] args) {
		new Emulator(args);
	}
}
