/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hk.quantr.riscv.cpu;

public class MemoryMap {
	public String name;
	public long start;
	public long end;
	
	public MemoryMap(String name, long start, long end) {
		this.name = name;
		this.start = start;
		this.end = end;
	}
}
