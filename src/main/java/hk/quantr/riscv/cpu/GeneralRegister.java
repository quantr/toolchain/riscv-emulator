package hk.quantr.riscv.cpu;

public class GeneralRegister implements Register{
	
	public String name;
	public String type;
	public long value;

	public GeneralRegister(String name, String type, long value) {
		this.name = name;
		this.type = type;
		this.value = value;
	}
	
	@Override
	public void setValue(long value) {
		this.value = value;
	}
	
	@Override
	public void setValue(String value, int radix) {
		this.value = Long.parseUnsignedLong(value, radix);
	}

	@Override
	public long getValue() {
		return value;
	}

	@Override
	public String getRegisterValue() {
		return String.format("%15s = %016x", name, value);
	}

	@Override
	public String getHexValue() {
		return Long.toHexString(value);
	}
	
}
