/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hk.quantr.riscv.cpu;

import hk.quantr.riscv.exception.AccessFaultException;
import java.util.ArrayList;

public class Memory {
	
	public String value; //Used by memory settings, do not delete
	
	public static ArrayList<MemoryMap> memoryMaps = new ArrayList<>();
	public static byte[] mem1000 = new byte[61440];
	public static byte[] mem100000 = new byte[4128];
	public static byte[] mem200000 = new byte[49152];
	public static byte[] mem300000 = new byte[65536];
	public static byte[] mem10000000 = new byte[8];
	public static byte[] mem10001000 = new byte[29184];
	public static byte[] mem20000000 = new byte[67108864];
	public static byte[] mem30000000 = new byte[1342177280];
	public static byte[] mem80000000 = new byte[134217728];

	public Memory() {
		memoryMaps.add(new MemoryMap("mem1000", 0x1000, 0x10000));
		memoryMaps.add(new MemoryMap("mem100000", 0x100000, 0x101020));
		memoryMaps.add(new MemoryMap("mem200000", 0x2000000, 0x200c000));
		memoryMaps.add(new MemoryMap("mem300000", 0x3000000, 0x3010000));
		memoryMaps.add(new MemoryMap("mem10000000", 0x10000000, 0x10000008));
		memoryMaps.add(new MemoryMap("mem10001000", 0x10001000, 0x10008200));
		memoryMaps.add(new MemoryMap("mem20000000", 0x20000000, 0x24000000));
		memoryMaps.add(new MemoryMap("mem30000000", 0x30000000, 0x80000000L));
		memoryMaps.add(new MemoryMap("mem80000000", 0x80000000L, 0x88000000L));
	}
	
	public void write(long address, byte b) throws AccessFaultException {
		MemoryMap mem = getMemoryMap(address);
		if (b != 0) {
			switch (mem.name) {
				case "mem1000" -> { mem1000[(int)(address - mem.start)] = b; }
				case "mem100000" -> { mem100000[(int)(address - mem.start)] = b; }
				case "mem200000" -> { mem200000[(int)(address - mem.start)] = b; }
				case "mem300000" -> { mem300000[(int)(address - mem.start)] = b; }
				case "mem10000000" -> { mem10000000[(int)(address - mem.start)] = b; }
				case "mem10001000" -> { mem10001000[(int)(address - mem.start)] = b; }
				case "mem20000000" -> { mem20000000[(int)(address - mem.start)] = b; }
				case "mem30000000" -> { mem30000000[(int)(address - mem.start)] = b; }
				case "mem80000000" -> { mem80000000[(int)(address - mem.start)] = b; }
			}
		}
	}
	
	public void writeMemory(long address, long value, int numOfBytes, boolean physical) {
		
	}
	
	public byte read(long address) throws AccessFaultException {
		MemoryMap mem = getMemoryMap(address);
		switch(mem.name) {
			case "mem1000" -> { return mem1000[(int)(address - mem.start)]; }
			case "mem100000" -> { return mem100000[(int)(address - mem.start)]; }
			case "mem200000" -> { return mem200000[(int)(address - mem.start)]; }
			case "mem300000" -> { return mem300000[(int)(address - mem.start)]; }
			case "mem10000000" -> { return mem10000000[(int)(address - mem.start)]; }
			case "mem10001000" -> { return mem10001000[(int)(address - mem.start)]; }
			case "mem20000000" -> { return mem20000000[(int)(address - mem.start)]; }
			case "mem30000000" -> { return mem30000000[(int)(address - mem.start)]; }
			case "mem80000000" -> { return mem80000000[(int)(address - mem.start)]; }
			default -> { throw new AccessFaultException(); }
		}
	}
	
	public MemoryMap getMemoryMap(long address) throws AccessFaultException {
		for (MemoryMap mem : memoryMaps) {
			if (mem.start <= address && address < mem.end) {
				return mem;
			}
		}
		System.err.printf("Cannot access memory at 0x%x\n", address);
		throw new AccessFaultException();
	}
	
}
