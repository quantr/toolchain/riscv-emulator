/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package hk.quantr.riscv.cpu;


/*
	Several types of Register
	1. normal
	2. floating
	3. vector
	4. csr

	note:
	Hex String to long -> use Long.parseUnsignedLong(xxx, 16);
	long to Hex String -> use Long.toHexString(xxx); or getHexValue();
*/

public interface Register {
	
	void setValue(long value);
	void setValue(String value, int radix);
	long getValue();
	String getHexValue();
	String getRegisterValue();
	
}
