package hk.quantr.riscv.cpu;

public class CSRRegister implements Register{

	public String name;
	public String type = "csr";
	public int code;
	public long value;

	public CSRRegister(String name, int code, long value) {
		this.name = name;
		this.code = code;
		this.value = value;
	}
	
	@Override
	public void setValue(long value) {
		this.value = value;
	}
	
	@Override
	public void setValue(String value, int radix) {
		this.value = Long.parseUnsignedLong(value, radix);
	}

	@Override
	public long getValue() {
		return value;
	}

	@Override
	public String getRegisterValue() {
		return String.format("%15s = %016x", name, value);
	}

	@Override
	public String getHexValue() {
		return Long.toHexString(value);
	}

	
	
}
