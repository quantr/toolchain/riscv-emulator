/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hk.quantr.riscv.emulator;

import java.util.Scanner;

public class testLongType {
	
	static long value = 0;
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		while (true) {
			System.out.print(">");
			String command = in.nextLine().trim();
			if (command.equals("q")) break;
			else if (command.equals("p")) System.out.println(Long.toHexString(value));
			else {
				command = command.substring(2);
				value = Long.parseUnsignedLong(command, 16);
				value -= 0xf234567812345678l;
			}
		}
	}
}
