/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hk.quantr.riscv.emulator;

import hk.quantr.riscv.translator.Translator;
import java.io.File;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class TestTranslator {

	@Test
	public void test() throws Exception {

		System.out.println("Start translating...");
		Translator t = new Translator();
		t.start();
		byte[] b = t.translate();

		Loader l = new Loader();
		System.out.println("Start running...");
		FileUtils.writeByteArrayToFile(new File("Emulator.class"), b);
		Class c = l.defineClass("Emulator", b);
		Object instance = c.getDeclaredConstructor().newInstance();
		c.getMethod("runKernel").invoke(instance);

	}
}
